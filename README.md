# Project:  bank-presentation
	Practitioner Bootcamp TechU   2018 Edition.

## Autor:			
	
	MB65379 | MFMB | FerHarris

### Description:
 + Bank APP composed in 3 leyers
 	+ FrontEnd
 	+ BackEnd
 	+ DataBase
 	+ Externals APIs 

### Componentes Relacionados:
 + bank-api
 + bank-front
 + **[bank-presentation link aws s3 ](https://s3.us-west-2.amazonaws.com/mx-techui-pract-mbmf/bank-presentation/reveal-js/index.html)**  

## List of Requirements
