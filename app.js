'use strict';

//    Constants
const PORT 			= 3001;
const HOST 			= '127.0.0.1';
const PROJECT_TITLE	= 'Tech University 2018';
const PROJECT_NAME	= 'Bank-API | Doc | PPT';
const PROJECT_AUTOR	= 'Autor:  MB65379 | MFMB | FerHarris';

// The number of milliseconds in one day
const oneDay 		= 86400000;

// Package's & Moduls Dependencies		
const express 		= require('express');
const app 			= express();

// Use compress middleware to gzip content
app.use(express.compress());

// Serve up content from public directory
app.use('/',   express.static(__dirname + '/', { maxAge: oneDay }));
app.use('/doc',express.static(__dirname + '/reveal-js', { maxAge: oneDay }));


app.listen(process.env.PORT || PORT);
console.log('=========================================');     
console.log(` > ${PROJECT_TITLE}                      `);
console.log(` > ${PROJECT_NAME}                       `);
console.log(` > ${PROJECT_AUTOR}                      `);
console.log('=========================================');     
console.log('  ###            #         ##  ###  ###  ');
console.log('  #  #  ##  # #  #  #      ##  #  #  #   ');
console.log('  #  #    # ## # # #      #  # #  #  #   ');
console.log('  ###   ### #  # ##       #  # ###   #   ');
console.log('  #  # #  # #  # # #      #### #     #   ');
console.log('  #  # # ## #  # #  #     #  # #     #   ');
console.log('  ###   # # #  # #   #    #  # #    ###  ');
console.log('=========================================');
console.log(` >Running on http://${HOST}:${PORT}/doc/`);
console.log('========================================='); 


